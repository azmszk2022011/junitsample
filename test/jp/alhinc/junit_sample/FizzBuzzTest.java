package jp.alhinc.junit_sample;

import static org.junit.Assert.*;

import org.junit.Test;

public class FizzBuzzTest {

	/*引数に9を渡した場合、「Fizz」が取得できるテストケース*/
	@Test
	public void test1() {
		assertEquals("Fizz", FizzBuzz.checkFizzBuzz(9));
	}

	/*引数に20を渡した場合、「Buzz」が取得できるテストケース*/
	@Test
	public void test2() {
		assertEquals("Buzz", FizzBuzz.checkFizzBuzz(20));
	}

	/*引数に45を渡した場合、「FizzBuzz」が取得できるテストケース*/
	@Test
	public void test3() {
		assertEquals("FizzBuzz", FizzBuzz.checkFizzBuzz(45));
	}

	/*引数に44を渡した場合、「44」が取得できるテストケース*/
	@Test
	public void test4() {
		assertEquals("44", FizzBuzz.checkFizzBuzz(44));
	}

	/*引数に46を渡した場合、「46」が取得できるテストケース*/
	@Test
	public void test5() {
		assertEquals("46", FizzBuzz.checkFizzBuzz(46));
	}

}
